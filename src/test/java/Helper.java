import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Helper {

    public static void LoadTestCase(String fileName) throws IOException {
        InputStream targetStream = Helper.class.getClassLoader().getResourceAsStream(fileName);
        assert targetStream != null;
        System.setIn(new ByteArrayInputStream(targetStream.readAllBytes()));
    }
}
