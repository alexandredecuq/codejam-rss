import java.util.*;

/** rename to public class Solution for submission **/
public class Vestigium {
    static int duplicateInRows(int[][] m) {
        int cnt = 0;
        for (int i = 0; i < m.length; i++) {
            boolean[] exist = new boolean[m.length + 1];
            for (int j = 0; j < m.length; j++) {
                int value = m[i][j];
                if (exist[value]) {
                    cnt++;
                    break;
                }
                exist[value] = true;
            }
        }
        return cnt;
    }

    static int duplicateInColumns(int[][] m) {
        int cnt = 0;
        for (int i = 0; i < m.length; i++) {
            boolean[] exist = new boolean[m.length + 1];
            for (int j = 0; j < m.length; j++) {
                int value = m[j][i];
                if (exist[value]) {
                    cnt++;
                    break;
                }
                exist[value] = true;
            }
        }
        return cnt;
    }

    static int trace(int[][] m) {
        int cnt = 0;
        for (int i = 0; i < m.length; i++)
            cnt += m[i][i];
        return cnt;
    }

    static void solve(int[][] m) {
        System.out.print(trace(m) + " ");
        System.out.print(duplicateInRows(m) + " ");
        System.out.println(duplicateInColumns(m) + " ");
    }

    static int[][] readInput(Scanner sc, int N) {
        int[][] m = new int[N][N];
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                m[i][j] = sc.nextInt();
        return m;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int T = sc.nextInt();
        for (int t = 1; t <= T; t++) {
            int N = sc.nextInt();
            int[][] m = readInput(sc, N);
            System.out.print("Case #" + t + ": ");
            solve(m);
        }
    }
}